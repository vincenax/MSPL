---
title: "Projet Statistique"
author: "Axel Vincent & Pierre Berger"
date: "9 mars 2018"
output:
  pdf_document: 
    toc: true
---

```{r echo = F,setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r ,  include=FALSE}
# package
if (!require(dplyr)) install.packages('dplyr')
library(dplyr)
if (!require(ggplot2)) install.packages('ggplot2')
library(ggplot2)
if (!require(gridExtra)) install.packages('gridExtra')
library(gridExtra)
if (!require(plotly)) install.packages('plotly')
library(plotly)
if (!require(scales)) install.packages('scales')
library(scales)
if (!require(ggExtra)) install.packages('ggExtra')
library(ggExtra)

# Lecture du fichier
winedata <- read.csv("winemag-data_first150k.csv",encoding="UTF-8" )
# Suppression des variables non analysable
winedata <- subset(winedata, select = -c(description, X))
# Supprime les lignes n'appartenant à aucun pays ou du pays US-France qui n'est pas cohérent pour notre analyse
winedata <- winedata[!(winedata$country == c("US-France","")),]
# Suppression des lignes incomplètes
winedata <- winedata[complete.cases(winedata),]
# Suppression des doublons
winedata <- winedata %>% distinct
```
# Partie 1 : Introduction

Notre travail porte sur l'étude des vins dans le monde et leur comparaison avec plusieurs critères, leur localisation, leur prix ainsi qui leur note. De ce fait, nous allons travailler sur un jeu de données issu du site www.kaggle.com qui référencie 150 930 observations de vin. Ce jeu de données, référencé sur kaggle provient plus précisément du magasine Américain "Wine Enthusiast" qui a noté les vins du monde entier entre 1 et 100. Ensuite les notes inférieures à 80 ont été retirées afin de garder seulement des vins de "qualité".

Source data: https://www.kaggle.com/zynicide/wine-reviews
             https://www.winemag.com/
             
Dans le cadre de cette étude nous nous sommes posés cette question : 

### Pouvons nous dire qu'il y a une correlation significative entre le prix et la note d'un vin? 

\newpage

# Partie 2 : Réalisation
## Traitement des données


En observant les données nous avons remarqué qu'il y avait des variables qui ne nous intéressait pas pour notre étude ainsi que des données abérrantes. 
Notamment nous étions en présence d'une variable contenant les descriptions faites par des oeunologue de chaque vin, malgré son intérêt pour un sujet portant sur le text mining il ne nous était pas d'une grande utilité dans notre problématique. Egalement, nous avons décidé d'isoler une valeur qui nous paraissait abérrante, puisqu'un vin était attribué à un pays nommé "US-France", ce qui pouvait amener à fausser nos résultats lors de la comparaison entre les pays.
Enfin, après avoir manipulé les données sur quelques essais nous nous sommes rendu compte d'une grande quantités de données dupliquées.
Après nettoyage notre jeu de données est passé de 150 930 lignes et 11 variables à 86 539 lignes et 9 variables. 

```{r echo = F}
summary(winedata)
```
\newpage

## Observation des prix attribués selon les pays

```{r echo = F}
proper <- winedata %>% group_by(country) %>% summarise(Mprice = mean(price), Mpoints = mean(points), Sprice = sd(price), Spoints = sd(points))

```

L'objectif de cette première analyse était de montrer une corrélation entre le prix ainsi que le pays d'origine, ce qui nous a donné le graphique ci-dessous. Il s'agit d'un graphique en barre représentant le prix (en dollars) en fonction du pays d'origine du vin. Sur ce graphique nous avons également placés les intervalles de confiance. Les vins sont donc classés par pays et par prix, avec a gauche les pays ayant une moyenne des prix des vins de ce pays la plus élevée et a droite les pays ayant une moyenne des prix du vin la plus faible.
On peut donc remarquer que le pays ayant les vins les plus chère est le Royaume-Uni (England), la France se trouve 3ème. Grâce aux intervalles de confiance, on peut remarquer que les données sont peu homogène. En effet le Royaume-Uni a un intervalle de confiance de l'ordre de `r round(proper[proper$country == 'England',]$Sprice,2)`\$ alors que pour la France il est de `r round(proper[proper$country == 'France',]$Sprice,2)`\$. Cela est dû au fait que certains vins ont des prix très élevés ce qui a tendance a fausser les analyses.

```{r echo = F}
ggplot(data = proper, aes(x=reorder(country,-Mprice), y=Mprice)) + geom_bar(stat = "identity") + theme(axis.text.x=element_text(angle=-90, vjust=0.5,hjust=0)) + geom_errorbar(aes(ymin=Mprice-Sprice, ymax=Mprice+Sprice), width=.1)
```

\newpage

## Ditributions des points attribués au vin

Si nous regardons la distributions des points attribués au vin, nous pouvons observer une distribution gaussienne centrée autour de 87. Il y a seulement `r nrow(winedata[winedata$points == 100,])` vins qui atteignent le score idéal de 100. Parmis ceux-la, le plus accessible est le `En Chamberlin Syrah from Cayuse Vineyards` venant des Etats uni proche de Washington avec un prix affiché à `r min(winedata[winedata$points == 100,]$price)`\$. Et le moins accessible vient de France, c'est le `Clos du Mesnil` de la région Champagne avec un prix affiché à `r max(winedata[winedata$points == 100,]$price)`\$. On peut remarquer que les vins ayant une note supérieur à 90 ne sont pas en majorité. 

```{r echo = F, fig.width=15,fig.height=5}

g <- ggplot(winedata, aes(x = as.factor(winedata$points), fill = (..count..)/sum(..count..)*100)) +
  geom_bar(aes(y = (..count..)/sum(..count..))) +
  geom_text(aes(y = ((..count..)/sum(..count..)), label = scales::percent((..count..)/sum(..count..))), stat = "count", vjust = -0.25, hjust=.5, size = 4) +
  scale_y_continuous(labels = percent) +
  labs(title = "Distribution des points attribués au vin", y = "Pourcentage", x = "Points attribués") +
  theme(panel.background = element_rect(fill = "white", colour = "grey50")) + 
  scale_fill_gradient(low="#81CFE0", high="#446CB3")+ 
  labs(fill='Niveau en %')
g  
```

## Ditributions des prix attibués au vin

Lorsque l'on regarde la distribution des prix attribués au vin, on remarque que la grande majorité des vins ont un prix entre quelques dollars et 50\$. La part des vins supérieur à 50\$ est faible et plus le prix augmente et celle-ci devient inexistante.

```{r echo = F, fig.width=15,fig.height=5}
j <- ggplot(winedata, aes(x = as.factor(winedata$price), fill = (..count..)/sum(..count..)*100)) +
  geom_bar(aes(y = (..count..)/sum(..count..))) +
  scale_y_continuous(labels = percent) +
  labs(title = "Distribution des prix attribués au vin", y = "Pourcentage", x = "Prix attribués") +
  theme(panel.background = element_rect(fill = "white", colour = "grey50"),axis.text.x=element_text(angle=-90, vjust=0.5,hjust=0)) + 
  scale_fill_gradient(low="#81CFE0", high="#446CB3")+ 
  labs(fill='Niveau en %') + scale_x_discrete(breaks = c(seq(0, 2300, by=50)))
j
```

\newpage


## Comparaison des points attribués selon les points

Le graphique ci-dessous est un nuage de points auquel nous avons ajouté les fréquences observées pour chaque variable. Sans les fréquences le nuage de point pourrait fausser notre analyse, en effet on aurait tendance a dire que les vins ont une note supérieur à 90 avec un prix situé autour de la centaine de dollars. Grâce aux fréquences on peut croiser notre analyse des deux graphiques ci-dessus a ce nuage de point et confirmer le fait qu'il y ai une distribution gaussienne centrée autour de 87 concernant les notes et que pour le prix la plus part des vins sont accessibles à un prix inférieux à 50$. On ne peut pas tirer de conclusion concernant une relation note/prix même si l'on remarque que les prix ont tendance a a être plus dispersé lorsque la note est supérieur a 92.

```{r echo = F, fig.width=8,fig.height=6}
hist_top <- ggplot(winedata, aes(x = as.factor(winedata$points), fill = (..count..)/sum(..count..)*100)) +
  geom_bar(aes(y = (..count..)/sum(..count..))) +
  scale_y_continuous(labels = percent) +
  scale_fill_gradient(low="#81CFE0", high="#446CB3") + 
  theme(
        panel.grid.major.y = element_blank(),
        panel.grid.minor.y = element_blank(),
        axis.ticks=element_blank(), 
        panel.background=element_blank(), 
        axis.text.x=element_blank(), axis.text.y=element_blank(),           
        axis.title.x=element_blank(), axis.title.y=element_blank(),
        legend.position = "none"
        ) +
  theme(plot.margin = margin(0, 0.52, 0, 1.95, "cm"))

empty <- ggplot()+geom_point(aes(1,1), colour="white")+
         theme(axis.ticks=element_blank(), 
               panel.background=element_blank(), 
               axis.text.x=element_blank(), axis.text.y=element_blank(),           
               axis.title.x=element_blank(), axis.title.y=element_blank())

scatter <- ggplot(winedata,aes(x=points, y=price)) + geom_point(alpha = 1/10)

hist_right <- ggplot(winedata, aes(x = as.factor(winedata$price), fill = (..count..)/sum(..count..)*100)) +
  geom_bar(aes(y = (..count..)/sum(..count..))) +
  scale_y_continuous(labels = percent) +
  scale_fill_gradient(low="#81CFE0", high="#446CB3") + 
  theme(
        panel.grid.major.y = element_blank(),
        panel.grid.minor.y = element_blank(),
        axis.ticks=element_blank(), 
        panel.background=element_blank(), 
        axis.text.x=element_blank(), axis.text.y=element_blank(),           
        axis.title.x=element_blank(), axis.title.y=element_blank(),
        legend.position = "none"
        )+
    theme(plot.margin = margin(4,0,1.6,0, "cm")) + coord_flip()

grid.arrange(hist_top, empty, scatter, hist_right, ncol=2, nrow=2, widths=c(4, 1), heights=c(1, 4))
```

\newpage
## Comparaison des points attribués selon les prix, borne de prix allant de 0 à 50$

Afin de pouvoir établir une relation entre la note et le prix nous avons décidé de supprimer les vins ayant un prix supérieur à 50\$ en effet la plupart des vins ayant un prix inférieur a 50\$ cela nous permet d'avoir des données plus hétérogènes. Le graphique ci-dessous est donc le même que celui du dessus mais avec des prix entre 0 et 50\$. Après ce nettoyage, nous gardons `r nrow(winedata[winedata$price<51,])/nrow(winedata)*100`% des données

En observant ce graphique on peut remarquer qu'il y a une corrélation entre le prix et la qualité du vin. Tout du moins, lorsque la note est élevé en effet lorsque la note est supérieur à 90, il y a moins de vins avec un prix faible. Mais lorsque la note est inférieur à 90, ce n'est pas pour autant que les prix sont tous faibles.

```{r echo = F, fig.width=8,fig.height=6}
winedata2 <- winedata[winedata$price<50,]
proper2 <- cbind(type = c("Price","Points"),rbind(winedata2  %>% summarise(mean = mean(price),sd = sd(price)),winedata  %>% summarise(mean = mean(points),sd = sd(points))))
Binf <- proper2[proper2$type=="Points",]$mean - proper2[proper2$type=="Points",]$sd


hist_top <- ggplot(winedata2, aes(x = as.factor(winedata2$points), fill = (..count..)/sum(..count..)*100)) +
  geom_bar(aes(y = (..count..)/sum(..count..))) +
  scale_y_continuous(labels = percent) +
  scale_fill_gradient(low="#81CFE0", high="#446CB3") + 
  theme(
        panel.grid.major.y = element_blank(),
        panel.grid.minor.y = element_blank(),
        axis.ticks=element_blank(), 
        panel.background=element_blank(), 
        axis.text.x=element_blank(), axis.text.y=element_blank(),           
        axis.title.x=element_blank(), axis.title.y=element_blank(),
        legend.position = "none"
        ) +
  theme(plot.margin = margin(0, 0.4, 0, 1.5, "cm"))

empty <- ggplot()+geom_point(aes(1,1), colour="white")+
         theme(axis.ticks=element_blank(), 
               panel.background=element_blank(), 
               axis.text.x=element_blank(), axis.text.y=element_blank(),           
               axis.title.x=element_blank(), axis.title.y=element_blank())

scatter <- ggplot(winedata2,aes(x=points, y=price)) + geom_point(alpha = 1/10) + expand_limits( y = 0)

hist_right <- ggplot(winedata2, aes(x = as.factor(winedata2$price), fill = (..count..)/sum(..count..)*100)) +
  geom_bar(aes(y = (..count..)/sum(..count..))) +
  scale_y_continuous(labels = percent) +
  scale_fill_gradient(low="#81CFE0", high="#446CB3") + 
  theme(
        panel.grid.major.y = element_blank(),
        panel.grid.minor.y = element_blank(),
        axis.ticks=element_blank(), 
        panel.background=element_blank(), 
        axis.text.x=element_blank(), axis.text.y=element_blank(),           
        axis.title.x=element_blank(), axis.title.y=element_blank(),
        legend.position = "none"
        )+
    theme(plot.margin = margin(0.65,0,2.5,0, "cm")) + coord_flip()

grid.arrange(hist_top, empty, scatter, hist_right, ncol=2, nrow=2, widths=c(4, 1), heights=c(1, 4))
```

Nous avons donc souhaité dresser les intervalles de confiances associées aux points et aux prix :\newline

Intervalle de confiance des points : \[ `r proper2[proper2$type=="Points",]$mean - proper2[proper2$type=="Points",]$sd`;`r proper2[proper2$type=="Points",]$mean + proper2[proper2$type=="Points",]$sd`\] \newline
Intervalle de confiance des prix : \[`r proper2[proper2$type=="Price",]$mean - proper2[proper2$type=="Price",]$sd`;`r proper2[proper2$type=="Price",]$mean + proper2[proper2$type=="Price",]$sd`\] \newline

```{r echo = F, fig.width=4,fig.height=3}
ggplot(data = proper2, aes(type, mean, label = round(mean,2))) + geom_bar(stat = "identity", aes(fill=type))  + geom_errorbar(aes(ymin=mean-sd, ymax=mean+sd), width=.4)+
  theme_bw() + scale_y_continuous(labels = dollar)  + geom_text(size = 3, position = position_stack(vjust = 0.3))
```

##Conclusion

Cette analyse nous montre que la majorité des vins dans ont obtenu une note entre 84,6 et 91,1 points et se situent dans une fourchette de prix allant de 12,7 à 33,9\$. Le coefficient de correlation entre les deux variable est de `r cor(winedata2$points,winedata2$price)`, cela nous prouve qu'il y a bien une corrélation modérée entre la note et le prix d'un vin.

De ce fait nous pouvons répondre à la problématique. Oui il y a une corrélation entre le prix et la qualité d'un vin, quand la note de celui-ci est élevé le prix a tendance à être plus élevé qu'habituellement. Parcontre ce n'est pas parce que le vin est cher que la note sera élevé, et dans le sens inverse, il est possible de trouver des vins de très bonne qualité à des prix faibles.

Ce jeu de données n'était pas facile à analyser, en effet les données ne sont pas forcément de bonne qualité et nous aurions aimé aller plus loin. Notamment au niveau de la localisation des vins si nous avions eu des coordonnées exactes par exemple. Nous aurions idéalement pu analyser ce jeu de données grâce a du text mining afin de créer de nouvelles relations en analysant les descriptions. Nous avons également manqué de temps et de compétences afin d'aller plus profondément dans une démarche d'analyse complète.

Après cette étude nous sommes donc en mesure de nous demander si il existe d'autres facteurs déterminant pour l'élaboration d'un prix d'un vin? (Quantité, localité, sépage, rareté, couleur...)

